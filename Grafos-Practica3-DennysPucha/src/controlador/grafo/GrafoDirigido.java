/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.grafo;

import controlador.grafo.exception.VerticeOfSizeException;
import controlador.listas.ListaEnlazada;
import controlador.listas.colas.Cola;
import controlador.listas.colas.Exceptions.ColaLlenaException;
import controlador.listas.colas.Exceptions.ColaVaciaException;
import controlador.listas.excepciones.ListaNullaException;
import controlador.listas.excepciones.PosicionNoEncontradaException;
import controlador.listas.pilas.Exceptions.PilaLlenaException;
import controlador.listas.pilas.Exceptions.PilaVaciaException;
import controlador.listas.pilas.Pila;

/**
 *
 * @author Dennys
 */
public class GrafoDirigido extends Grafo {

    private Integer numVertices;
    private Integer numAristas;
    private ListaEnlazada<Adyacencia> listaAdycente[];
    private Double distancias[][];
    private double[] distanciaAux;
    private Integer[] camino;

    public GrafoDirigido(Integer numVertices) {
        this.numVertices = numVertices;
        numAristas = 0;
        listaAdycente = new ListaEnlazada[numVertices + 1];
        for (int i = 1; i <= this.numVertices; i++) {
            listaAdycente[i] = new ListaEnlazada<>();
        }
    }

    @Override
    public Integer numVertices() {
        return numVertices;
    }

    @Override
    public Integer numAristas() {
        return numAristas;
    }

    @Override
    public Boolean existeArista(Integer o, Integer d) throws Exception {
        Boolean existe = false;
        if (o.intValue() <= numVertices && d.intValue() <= numVertices) {
            ListaEnlazada<Adyacencia> lista = listaAdycente[o];
            for (int i = 0; i < lista.getSize(); i++) {
                Adyacencia a = lista.obtener(i);
                if (a.getDestino().intValue() == d.intValue()) {
                    existe = true;
                    break;
                }
            }
        } else {
            throw new VerticeOfSizeException();
        }
        return existe;
    }

    @Override
    public Double pesoArista(Integer o, Integer d) {
        Double peso = Double.NaN;
        try {
            if (existeArista(o, d)) {
                ListaEnlazada<Adyacencia> adycentes = listaAdycente[o];
                for (int i = 0; i < adycentes.getSize(); i++) {
                    Adyacencia a = adycentes.obtener(i);
                    if (a.getDestino().intValue() == d.intValue()) {
                        peso = a.getPeso();
                        break;
                    }
                }
            }
        } catch (Exception e) {
        }
        return peso;
    }

    @Override
    public void insertarArista(Integer o, Integer d, Double peso) throws Exception {

        if (o.intValue() <= numVertices && d.intValue() <= numVertices) {
            if (!existeArista(o, d)) {
                numAristas++;
                listaAdycente[o].insertar(new Adyacencia(d, peso));
            }
        } else {
            throw new VerticeOfSizeException();
        }

    }

    @Override
    public void insertarArista(Integer o, Integer d) throws Exception {
        insertarArista(o, d, Double.NaN);
    }

    @Override
    public ListaEnlazada<Adyacencia> adycentes(Integer v) {
        return listaAdycente[v];
    }

    public Integer getNumVertices() {
        return numVertices;
    }

    public Integer getNumAristas() {
        return numAristas;
    }

    public void setNumAristas(Integer numAristas) {
        this.numAristas = numAristas;
    }

    public ListaEnlazada<Adyacencia>[] getListaAdycente() {
        return listaAdycente;
    }

    public ListaEnlazada dijkstra(Integer inicio) throws ListaNullaException, PosicionNoEncontradaException {
        ListaEnlazada resultado = new ListaEnlazada();
        boolean[] AristaVisitada = new boolean[numVertices() + 1];
        distanciaAux = new double[numVertices() + 1];
        camino = new Integer[numVertices() + 1];

        for (int i = 0; i < camino.length; i++) {
            camino[i] = -1;
        }
        for (int i = 0; i < distanciaAux.length; i++) {
            distanciaAux[i] = Double.POSITIVE_INFINITY;
        }
        distanciaAux[inicio] = 0;

        for (int i = 0; i < numVertices(); i++) {
            int actual = DistanciaMinima(distanciaAux, AristaVisitada);
            AristaVisitada[actual] = true;

            ListaEnlazada<Adyacencia> listaAdj = adycentes(actual);
            for (int j = 0; j < listaAdj.getSize(); j++) {
                Adyacencia edge = listaAdj.obtener(j);
                int dest = edge.getDestino();
                double peso = edge.getPeso();

                if (!AristaVisitada[dest] && distanciaAux[actual] + peso < distanciaAux[dest]) {
                    distanciaAux[dest] = distanciaAux[actual] + peso;
                    camino[dest] = actual;
                }
            }
        }

        for (int i = 1; i < distanciaAux.length; i++) {
            resultado.insertar(distanciaAux[i]);
        }
        return resultado;
    }

//    public void DijkstraRepresentarCaminosPosibles() {
//        for (int i = 1; i < distanciaAux.length; i++) {
//            System.out.print("Camino recorrido hacia el vértice " + i + ": ");
//            ImprimirCaminoDijkstra(i);
//            System.out.println();
//        }
//        System.out.println();
//    }
    public ListaEnlazada<ListaEnlazada<Integer>> DijkstraRepresentarCaminosPosibles(Integer inicio) throws ListaNullaException, PosicionNoEncontradaException {
        dijkstra(inicio);
        ListaEnlazada<ListaEnlazada<Integer>> caminos = new ListaEnlazada<>();
        for (int i = 1; i < distanciaAux.length; i++) {
            ListaEnlazada<Integer> camino = new ListaEnlazada<>();
            ImprimirCaminoDijkstra(i, camino);
            caminos.insertar(camino);
        }
        return caminos;
    }

    private void ImprimirCaminoDijkstra(int destino, ListaEnlazada<Integer> caminoList) {
        Integer[] aux = camino;
        if (aux[destino] != -1) {
            ImprimirCaminoDijkstra(aux[destino], caminoList);
        }
        caminoList.insertarCabecera(destino);
    }

    private static int DistanciaMinima(double[] distancia, boolean[] AristaVisitada) {
        double min = Double.POSITIVE_INFINITY;
        int minIndex = -1;

        for (int i = 0; i < distancia.length; i++) {
            if (!AristaVisitada[i] && distancia[i] <= min) {
                min = distancia[i];
                minIndex = i;
            }
        }

        return minIndex;
    }

//    private void ImprimirCaminoDijkstra(int destino) {
//        Integer[] aux = camino;
//        if (aux[destino] != -1) {
//            ImprimirCaminoDijkstra(aux[destino]);
//        }
//        System.out.print(destino + " ");
//    }
    public Double obtenerDistanciadijkstra(Integer inicio, Integer destino) throws ListaNullaException, PosicionNoEncontradaException {

        ListaEnlazada lista = dijkstra(inicio);
        Double aux = 0.0;
        for (int i = 1; i < lista.getSize(); i++) {
            aux = (Double) lista.obtener(destino - 1);
        }
        return aux;
    }

    public void floyd() throws Exception {
        distancias = new Double[numVertices + 1][numVertices + 1];
        for (int i = 1; i <= numVertices; i++) {
            for (int j = 1; j <= numVertices; j++) {
                if (i == j) {
                    distancias[i][j] = 0.0;
                } else if (existeArista(i, j)) {
                    distancias[i][j] = pesoArista(i, j);
                } else {
                    distancias[i][j] = Double.POSITIVE_INFINITY;
                }
            }
        }
        for (int k = 1; k <= numVertices; k++) {
            for (int i = 1; i <= numVertices; i++) {
                for (int j = 1; j <= numVertices; j++) {
                    if (distancias[i][k] + distancias[k][j] < distancias[i][j]) {
                        distancias[i][j] = distancias[i][k] + distancias[k][j];
                    }
                }
            }
        }
        for (int k = 1; k <= numVertices; k++) {
            for (int i = 1; i <= numVertices; i++) {
                for (int j = 1; j <= numVertices; j++) {
                    if (distancias[i][k] + distancias[k][j] < distancias[i][j]) {
                        distancias[i][j] = distancias[i][k] + distancias[k][j];
                    }
                }
            }
        }
    }

    public ListaEnlazada<Integer> obtenerCaminoFloyd(int origen, int destino) throws Exception {
        floyd();
        ListaEnlazada<Integer> camino = new ListaEnlazada<>();
        if (distancias[origen][destino] == Double.POSITIVE_INFINITY) {
            return camino;
        }
        int verticeActual = destino;
        camino.insertarCabecera(verticeActual);
        while (verticeActual != origen) {
            for (int i = 1; i <= numVertices; i++) {
                if (distancias[origen][i] + pesoArista(i, verticeActual) == distancias[origen][verticeActual]) {
                    verticeActual = i;
                    camino.insertarCabecera(verticeActual);
                    break;
                }
            }
        }
        return camino;
    }

    public ListaEnlazada<Double> distanciasHaciaTodosLosVerticesFloyd(Integer inicio) throws Exception {
        floyd();
        ListaEnlazada<Double> listaDistancias = new ListaEnlazada<>();
        listaDistancias.insertar(0.0);
        for (int i = 1; i <= numVertices; i++) {
            if (i != inicio) {
                listaDistancias.insertar(distancias[inicio][i]);
            }
        }
        return listaDistancias;
    }



    public Double obtenerDistanciaFloyd(Integer o, Integer d) throws ListaNullaException, PosicionNoEncontradaException, Exception {
        floyd();
        return distancias[o][d];
    }

    public ListaEnlazada recorridoEnAnchura(Integer origen) throws ColaLlenaException, PosicionNoEncontradaException, ColaVaciaException, ListaNullaException {
        ListaEnlazada<Integer> recorrido = new ListaEnlazada<>();

        Boolean[] visitados = new Boolean[numVertices() + 1];
        for (int i = 1; i <= numVertices(); i++) {
            visitados[i] = false;
        }
        Cola<Integer> cola = new Cola<>(numVertices());
        visitados[origen] = true;
        cola.queue(origen);
        while (!cola.estaVacia()) {

            origen = cola.dequeue();
            recorrido.insertar(origen);
            ListaEnlazada<Adyacencia> adyacentes = adycentes(origen);
            for (int i = 0; i < adyacentes.getSize(); i++) {
                Adyacencia a = adyacentes.obtener(i);
                if (!visitados[a.getDestino()]) {
                    visitados[a.getDestino()] = true;
                    cola.queue(a.getDestino());
                }
            }
        }
        return recorrido;
    }

    public ListaEnlazada recorridoEnProfundidad(Integer origen) throws PilaLlenaException, PilaVaciaException, ListaNullaException, PosicionNoEncontradaException {
        ListaEnlazada<Integer> recorrido = new ListaEnlazada<>();
        Boolean[] visitados = new Boolean[numVertices() + 1];
        for (int i = 1; i <= numVertices(); i++) {
            visitados[i] = false;
        }

        Pila<Integer> pila = new Pila<>(numVertices());
        pila.push(origen);
        visitados[origen] = true;

        while (!pila.estaVacia()) {
            Integer vertice = pila.pop();
            recorrido.insertar(vertice);
            ListaEnlazada<Adyacencia> adyacentes = adycentes(vertice);
            for (int i = 0; i < adyacentes.getSize(); i++) {
                Adyacencia a = adyacentes.obtener(i);
                Integer destino = a.getDestino();
                if (!visitados[destino]) {
                    pila.push(destino);

                    visitados[destino] = true;
                }
            }
        }
        return recorrido;
    }
}
