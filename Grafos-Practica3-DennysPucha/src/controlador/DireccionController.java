/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import controlador.grafo.Adyacencia;
import controlador.grafo.GrafoNoDirijidoEtiquetado;
import controlador.listas.ListaEnlazada;
import java.lang.ProcessBuilder.Redirect.Type;
import java.util.HashMap;
import modelo.Direccion;

/**
 *
 * @author Dennys
 */
public class DireccionController {

    private Direccion direccion;
    private DAODireccionCasas daoCasa = new DAODireccionCasas("Casas");
    private GrafoNoDirijidoEtiquetado<Direccion> grafo;

    private void crearGrafoLista() throws Exception {
        ListaEnlazada<Direccion> lista = daoCasa.obtenerLista();
        grafo = new GrafoNoDirijidoEtiquetado<>(lista.getSize(), Direccion.class);
        for (int i = 0; i < lista.getSize(); i++) {
            grafo.etiquetarVertice((i + 1), lista.obtener(i));
        }
    }

    public GrafoNoDirijidoEtiquetado<Direccion> getGrafo() throws Exception {
        if (grafo == null) {
            crearGrafoLista();
        }
        return grafo;
    }

    public void setGrafo(GrafoNoDirijidoEtiquetado<Direccion> grafo) {
        this.grafo = grafo;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public Double calcularDistancia(Integer or, Integer de) throws Exception {
        Direccion o = getGrafo().obtenerEtiqueta(or);
        Direccion d = getGrafo().obtenerEtiqueta(de);
        //Double y = o.getLatitud() - d.getLatitud();
        //Double x = o.getLongitud() - d.getLongitud();
        return controlador.Utilidades.Utilidades.carcularDistancia(o.getLatitud(), d.getLatitud(), o.getLongitud(), d.getLongitud());
    }

    public void guardarGrafo() throws Exception {
        Gson gson = new Gson();
        ListaEnlazada<HashMap> verticesE = new ListaEnlazada<>();
        HashMap<Integer, ObjetoGrafo> mapa = new HashMap<>();
        for (int i = 1; i <= getGrafo().getNumVertices(); i++) {
            ObjetoGrafo obj = new ObjetoGrafo();
            obj.setId_clase(getGrafo().obtenerEtiqueta(i).getId());
            obj.setClase(getGrafo().obtenerEtiqueta(i).getClass().toString());
            obj.setListaAdycencias(getGrafo().adycentes(i));
            mapa.put(i, obj);
        }
        verticesE.insertar(mapa);
        controlador.Utilidades.Utilidades.guardarArchivo(gson.toJson(verticesE), "data/grafo.json");
    }

    public void cargarGrafo() throws Exception {
        Gson gson = new Gson();
        String jsonGrafo = controlador.Utilidades.Utilidades.leerArchivo("data/grafo.json");
        java.lang.reflect.Type type = new TypeToken<ListaEnlazada<HashMap>>() {
        }.getType();
        ListaEnlazada<HashMap> verticesE = gson.fromJson(jsonGrafo, type);
        HashMap<Integer, ObjetoGrafo> mapa = verticesE.obtener(0);
        ListaEnlazada<Direccion> lista = daoCasa.obtenerLista();
        grafo = new GrafoNoDirijidoEtiquetado<>(lista.getSize(), Direccion.class);
        for (int i = 1; i <= mapa.size(); i++) {
            grafo.etiquetarVertice(i, lista.obtener(mapa.get(i).getId_clase() - 1));
        }
    }

    class ObjetoGrafo {

        String clase;
        Integer id_clase;
        ListaEnlazada<Adyacencia> listaAdycencias = new ListaEnlazada<>();

        public String getClase() {
            return clase;
        }

        public void setClase(String clase) {
            this.clase = clase;
        }

        public Integer getId_clase() {
            return id_clase;
        }

        public void setId_clase(Integer id_clase) {
            this.id_clase = id_clase;
        }

        public ListaEnlazada<Adyacencia> getListaAdycencias() {
            return listaAdycencias;
        }

        public void setListaAdycencias(ListaEnlazada<Adyacencia> listaAdycencias) {
            this.listaAdycencias = listaAdycencias;
        }

    }
}
