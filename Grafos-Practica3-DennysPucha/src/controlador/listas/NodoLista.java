package controlador.listas;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Dennys
 */

public class NodoLista <E> {
    @JsonProperty("dato")
    private E dato;
    @JsonProperty("siguiente")
    private NodoLista<E> siguiente;

    public NodoLista (E dato, NodoLista<E> sig) {
        this.dato = dato;
        this.siguiente = sig;
    }

    public NodoLista () {
        this.dato = null;
        this.siguiente = null;
    }

    public E getDato() {
        return this.dato;
    }

    public void setDato(E dato) {
        this.dato = dato;
    }
    
    public NodoLista<E> getSiguiente() {
        return this.siguiente;
    }

    public void setSiguiente(NodoLista<E> siguiente) {
        this.siguiente = siguiente;
    }

}