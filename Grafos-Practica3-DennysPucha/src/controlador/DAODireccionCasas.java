/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import controlador.listas.ListaEnlazada;
import controlador.listas.excepciones.ListaNullaException;
import controlador.listas.excepciones.PosicionNoEncontradaException;
import java.io.File;
import java.io.IOException;
import modelo.Casa;
import modelo.Direccion;

/**
 *
 * @author Dennys
 */
public class DAODireccionCasas {
    private ListaEnlazada<Direccion> data;
    private ObjectMapper mapper;
    private File dataFile;

    public DAODireccionCasas(String fileName) {
        data = new ListaEnlazada<>();
        mapper = new ObjectMapper();
        dataFile = new File("data/" + fileName + ".json");
        cargar();
    }

    private void cargar() {
        try {
            data = mapper.readValue(dataFile, new TypeReference<ListaEnlazada<Direccion>>() {
            });
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void guardar() {
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(dataFile, data);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void insertar(Direccion direccion) {
        data.insertar(direccion);
    }

    public Direccion obtener(Integer pos) throws ListaNullaException, PosicionNoEncontradaException {
        return data.obtener(pos);
    }

    public void eliminar(Integer pos) throws ListaNullaException, PosicionNoEncontradaException {
        data.eliminar(pos-1);
    }

    public Integer obtenerTamanioLista() {
        return data.getSize();
    }

    public ListaEnlazada<Direccion> obtenerLista() {
        cargar();
        return data;
    }

    public void Editar(Integer pos,Direccion direccion) {
        data.modificarPoscicion(direccion, pos);
    }
}
