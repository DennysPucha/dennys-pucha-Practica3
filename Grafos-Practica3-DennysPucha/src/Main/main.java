/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;


import controlador.grafo.GrafoDirigido;
import controlador.grafo.GrafoDirigidoEtiquetado;
import controlador.grafo.GrafoNoDirigido;
import controlador.grafo.GrafoNoDirijidoEtiquetado;
import java.util.HashMap;
import modelo.Casa;
import vistas.FrmGrafo;
/**
 *
 * @author Dennys
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        // TODO code application logic here 
////        GrafoDirigido gd = new GrafoDirigido(4);
////        System.out.println(gd);
////        System.out.println("-------------------------");
////        try {
////            gd.insertarArista(4, 2);
////            gd.insertarArista(4, 1);
////            gd.insertarArista(4, 3);
////           // gd.insertarArista(2, 3);
////            System.out.println(gd);
////        } catch (Exception e) {
////            System.out.println("Error "+e.getMessage());
////        }
////        System.out.println("GRAFO NO DIRIGIDO");
////        GrafoNoDirigido gnd = new GrafoNoDirigido(4);
////        System.out.println(gnd);
////        System.out.println("-------------------------");
////        try {
////            gnd.insertarArista(1, 3);
////            gnd.insertarArista(1, 4);
////            gnd.insertarArista(2, 3);
////         //   new FrmGrafo(null, true, gnd).setVisible(true);
////            //gd.insertarArista(2, 3);
////            System.out.println(gnd);
////        } catch (Exception e) {
////            System.out.println("Error "+e.getMessage());
////        }
////        
////        HashMap<Integer, String> mapa = new HashMap<>();
////        mapa.put(5, "casa");
////        System.out.println(mapa.get(1));
//        GrafoNoDirijidoEtiquetado gde = new GrafoNoDirijidoEtiquetado(5, String.class);
//        gde.etiquetarVertice(1, "Mayuri");//
//        gde.etiquetarVertice(2, "Alice");
//        gde.etiquetarVertice(3, "Vanessa");
//        gde.etiquetarVertice(4, "Letty");//
//        gde.etiquetarVertice(5, "Cobos");
//        try {
//
//            gde.insertarAristaE(gde.obtenerEtiqueta(1), gde.obtenerEtiqueta(2), 25.0);
//
//            gde.insertarAristaE(gde.obtenerEtiqueta(3), gde.obtenerEtiqueta(5), 1.0);
//            gde.insertarAristaE(gde.obtenerEtiqueta(3), gde.obtenerEtiqueta(4), -10.0);
//            gde.insertarAristaE(gde.obtenerEtiqueta(2), gde.obtenerEtiqueta(3), 55.0);
//            gde.insertarAristaE(gde.obtenerEtiqueta(1), gde.obtenerEtiqueta(4), 1000.0);
//            gde.insertarAristaE(gde.obtenerEtiqueta(5), gde.obtenerEtiqueta(2), 10.0);
////            //System.out.println(gde.caminiMinimo(1, 4));
////            //gde.dijkstra(5).imprimir();
////            //gde.caminiMinimo(1, 4).imprimir();
////            //System.out.println(gde.toString());
////            //System.out.println(gde.obtenerDistanciadijkstra(1, 4));
////            gde.FloydpresentarCaminosPosibles();
////                GrafoDirigido gd=new GrafoDirigido(5);
////                gd.insertarArista(1, 3, 25.2);
////                gd.insertarArista(2, 4, 35.0);
////                gd.insertarArista(1, 5, 50.2);
////                gd.insertarArista(3, 2, 60.0);
////                gd.insertarArista(1, 4, 92.6);
////                gd.dijkstra(1).imprimir();
////              gde.recorridoEnProfundidad(4).imprimir();
////            floyd fy=new floyd(gde);
////            fy.camino(1, 4);
//            //gde.FloydpresentarCaminosPosibles();
//            gde.recorridoEnAnchura(1).imprimir();
//            //gde.recorridoEnProfundidad(1);
        GrafoDirigidoEtiquetado gde = new GrafoDirigidoEtiquetado(5, String.class);
        gde.etiquetarVertice(1, "Mayuri");//
        gde.etiquetarVertice(2, "Alice");
        gde.etiquetarVertice(3, "Vanessa");
        gde.etiquetarVertice(4, "Letty");//
        gde.etiquetarVertice(5, "Cobos");
        try {

            gde.insertarAristaE(gde.obtenerEtiqueta(1), gde.obtenerEtiqueta(2), 25.0);

            gde.insertarAristaE(gde.obtenerEtiqueta(3), gde.obtenerEtiqueta(5), 1.0);
            gde.insertarAristaE(gde.obtenerEtiqueta(3), gde.obtenerEtiqueta(4), -10.0);
            gde.insertarAristaE(gde.obtenerEtiqueta(2), gde.obtenerEtiqueta(3), 55.0);
            gde.insertarAristaE(gde.obtenerEtiqueta(1), gde.obtenerEtiqueta(4), 1000.0);
            gde.insertarAristaE(gde.obtenerEtiqueta(5), gde.obtenerEtiqueta(2), 10.0);
            //gde.DijkstraRepresentarCaminosPosibles(1).obtener(3).imprimir();
            //gde.obtenerCaminoFloyd(2, 4).imprimir();
            //gde.distanciasHaciaTodosLosVerticesFloyd(1).imprimir();
            gde.distanciasHaciaTodosLosVerticesFloyd(1).imprimir();
            
            new FrmGrafo(null, true, gde, 1).setVisible(true);
        } catch (Exception e) {
            System.out.println(e);
        }
//        DAOCasa dao=new DAOCasa("Casas");
//        Casa c1=new Casa();
//        c1.setColor("gris");
//        c1.setNombreDuenio("Mini");
//        c1.setNumCasa("15");
//        c1.setPisos(3);
//        c1.setId(dao.obtenerTamanioLista()+1);
//        try {
//            dao.insertar(c1);
//            dao.guardar();
//            System.out.println(dao.obtener(2));
//            dao.obtenerLista().imprimir();
//        } catch (Exception e) {
//        }
    }
}
