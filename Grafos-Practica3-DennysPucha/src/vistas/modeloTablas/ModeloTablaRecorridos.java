/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vistas.modeloTablas;

import controlador.DireccionController;
import controlador.listas.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Direccion;

/**
 *
 * @author Dennys
 */
public class ModeloTablaRecorridos extends AbstractTableModel{
    ListaEnlazada<Integer> lista=new ListaEnlazada<>();
    private DireccionController direccionController = new DireccionController(); 
    public ListaEnlazada<Integer> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Integer> lista) {
        this.lista = lista;
    }

    public DireccionController getDireccionController() {
        return direccionController;
    }

    public void setDireccionController(DireccionController direccionController) {
        this.direccionController = direccionController;
    }
    
@Override
    public int getRowCount() {
        return lista.getSize();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            Integer e = lista.obtener(rowIndex);
            Direccion p=direccionController.getGrafo().obtenerEtiqueta(lista.obtener(rowIndex));

            switch (columnIndex) {
            case 0: return (e != null)? e : "NO DEFINIDO";
            case 1: return (e != null)? p.toString() : "NO DEFINIDO";
           default: return null;
            }

        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        try {
            switch (column) {
            case 0: return "Vertice";
            case 1: return "Etiqueta";   
                default: return null;
            }

        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
    
}
