/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vistas.modeloTablas;

import controlador.listas.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Direccion;

/**
 *
 * @author Dennys
 */
public class ModeloTablaCasa extends AbstractTableModel{

    ListaEnlazada<Direccion> lista=new ListaEnlazada<>();

    public ListaEnlazada<Direccion> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Direccion> lista) {
        this.lista = lista;
    }

    
    @Override
    public int getRowCount() {
        return lista.getSize();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            Direccion e = lista.obtener(rowIndex);
            switch (columnIndex) {
            case 0: return (e != null)? e.getId() : "NO DEFINIDO";
            case 1: return (e != null)? e.getCasa().getColor() : "NO DEFINIDO";   
            case 2: return (e != null)? e.getCasa().getNombreDuenio() : "NO DEFINIDO";
            case 3: return (e != null)? e.getCasa().getNumCasa() : "NO DEFINIDO";
            case 4: return (e != null)? e.getCasa().getPisos(): "NO DEFINIDO";
            case 5: return (e != null)? e.getLatitud(): "NO DEFINIDO";
            case 6: return (e != null)? e.getLongitud(): "NO DEFINIDO";
           default: return null;
            }

        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        try {
            switch (column) {
            case 0: return "ID";
            case 1: return "COLOR";   
            case 2: return "Nombre Duenio";
            case 3: return "Numero Casa";
            case 4: return "Numero Pisos";
            case 5: return "Latitud";
            case 6: return "Longitud";

                default: return null;
            }

        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
    
}
