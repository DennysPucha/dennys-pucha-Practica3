/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.modeloTablas;

import controlador.DAODireccionCasas;
import controlador.listas.ListaEnlazada;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import modelo.Direccion;


/**
 *
 * @author Dennys
 */
public class UtilidadesVista {

    private static Integer inicial;


    public static void cargarComboDireccion(JComboBox cbx) throws Exception {
        cbx.removeAllItems();
        ListaEnlazada<Direccion> lista = new DAODireccionCasas("Casas").obtenerLista();
        for (int i = 0; i < lista.getSize(); i++) {
            cbx.addItem(lista.obtener(i));
        }

    }



    public static void cambiarTitutloTabla(JTable tabla, String valor) {
        JTableHeader tableHeader = tabla.getTableHeader();
        TableColumnModel tableColumnModel = tableHeader.getColumnModel();
        TableColumn tableColumn = tableColumnModel.getColumn(0);
        tableColumn.setHeaderValue(valor);
        tableHeader.repaint();
    }

    public static void cambiarColorFila(JTable tabla, Integer estado) {

        Integer cont = 1;
        Integer inicial = tabla.getRowCount() - 1;
        for (int i = 0; i < estado; i++) {
            tabla.setValueAt(cont, inicial, 0);
            DefaultTableCellRenderer render = (DefaultTableCellRenderer) tabla.getCellRenderer(inicial, 0);
            //Component c = tabla.prepareRenderer(render, inicial, 0);
            render.setBackground(Color.ORANGE);

            inicial--;
            cont++;

        }

        tabla.updateUI();
    }

    public static void permitirSoloNumTxt(JTextField txt) {
        txt.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!Character.isDigit(c)) {
                    e.consume();
                }
            }
        });
    }

    public static void permitirSoloLetrasTxt(JTextField txt) {
        txt.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (Character.isDigit(c)) {
                    e.consume();
                }
            }
        });
    }

    public static void limitarTamanioTxt(JTextField txt, Integer tamanio) {
        txt.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                if (txt.getText().length() >= tamanio) {
                    e.consume();
                    JOptionPane.showMessageDialog(txt, "Solo se permiten " + tamanio + " caracteres");
                }
            }
        });
    }
}
